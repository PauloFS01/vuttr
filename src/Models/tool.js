export default class tool {
    constructor (title = '', link = '', description = '', tags = []) {
        title = title;
        this.link = link;
        this.description = description;
        this.tags = tags;
    }
}