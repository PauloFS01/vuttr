import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import BootstrapVue from 'bootstrap-vue'
import { routes } from './routes';
import VueRouter from 'vue-router';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(VueResource); 
Vue.use(VueRouter);
Vue.use(BootstrapVue);


const router = new VueRouter ({
  routes
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
